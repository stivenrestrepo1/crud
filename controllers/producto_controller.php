<?php
	include dirname(__file__,2).'/models/producto.php';

	$productos = new Producto();

	//Request: creacion de un nuevo producto
	if(isset($_POST['create']))
	{
		if($productos->newProducto($_POST)){
			header('location: ../index.php?page1=newproducto&success=true');
		}else{
			header('location: ../index.php?page1=newproducto&error=true');
		}
	}

	//Request: editar producto
	if(isset($_POST['editproducto']))
	{
		if($productos->setEditProducto($_POST)){
			header('location: ../index.php?page1=editproducto&sku='.$_POST['sku'].'&success=true');
		}else{
			header('location: ../index.php?page1=editproducto&sku='.$_POST['sku'].'&error=true');
		}
	}

	//Request: eliminar producto
	if(isset($_GET['delete']))
	{
		if($productos->deleteProducto($_GET['sku'])){
			echo json_encode(["success"=>true]);
		}else{
			echo json_encode(["error"=>true]);
		}
	}

?>