<?php
	include dirname(__file__,2).'/models/tienda.php';

	$tiendas = new Tienda();

	//Request: creacion de una nueva tienda
	if(isset($_POST['create']))
	{
		if($tiendas->newTienda($_POST)){
			header('location: ../index.php?page=new&success=true');
		}else{
			header('location: ../index.php?page=new&error=true');
		}
	}

	//Request: editar tienda
	if(isset($_POST['edit']))
	{
		if($tiendas->setEditTienda($_POST)){
			header('location: ../index.php?page=edit&id_tienda='.$_POST['id_tienda'].'&success=true');
		}else{
			header('location: ../index.php?page=edit&id_tienda='.$_POST['id_tienda'].'&error=true');
		}
	}

	//Request: eliminar tienda
	if(isset($_GET['delete']))
	{
		if($tiendas->deleteTienda($_GET['id_tienda'])){
			echo json_encode(["success"=>true]);
		}else{
			echo json_encode(["error"=>true]);
		}
	}

?>