<?php
	include_once dirname(__file__,2)."/config/conexion.php";
	/**
	*
	*/
	class Producto
	{
		private $conn;
		private $link;

		function __construct()
		{
			$this->conn   = new Conexion();
			$this->link   = $this->conn->conectarse();
		}

		//Trae todos los productos registradas
		public function getProducto()
		{
			$query  ="SELECT * FROM producto";
			$result =mysqli_query($this->link,$query);
			$data   =array();
			while ($data[]=mysqli_fetch_assoc($result));
			array_pop($data);
			return $data;
		}

		//Crear una nueva tienda
		public function newProducto($data){
			$query  ="INSERT INTO producto (nombre, sku, descripcion, valor, tienda_fk) VALUES ('".$data['nombre']."','".$data['sku']."','".$data['descripcion']."','".$data['valor']."','".$data['tienda_fk']."')";
			$result =mysqli_query($this->link,$query);
			if(mysqli_affected_rows($this->link)>0){
				return true;
			}else{
				return false;
			}
		}

		//Obtener tienda por sku
		public function getProductoById($sku=NULL){
			if(!empty($sku)){
				$query  ="SELECT * FROM producto WHERE sku=".$sku;
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
			}else{
				return false;
			}
		}

		//Actulizar producto por sku
		public function setEditProducto($data){
			if(!empty($data['sku'])){
				$query  ="UPDATE producto SET nombre='".$data['nombre']."',descripcion='".$data['descripcion']."',valor='".$data['valor']."' WHERE sku=".$data['sku'];
				$result =mysqli_query($this->link,$query);
				if($result){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Borra la tienda por id
		public function deleteProducto($sku=NULL){
			if(!empty($sku)){
				$query  ="DELETE FROM producto WHERE sku=".$sku;
				$result =mysqli_query($this->link,$query);
				if(mysqli_affected_rows($this->link)>0){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

	}
