<?php
	include dirname(__file__,2)."/config/conexion.php";
	/**
	*
	*/
	class Tienda
	{
		private $conn;
		private $link;

		function __construct()
		{
			$this->conn   = new Conexion();
			$this->link   = $this->conn->conectarse();
		}

		//Trae todos las tiendas registradas
		public function getTienda()
		{
			$query  ="SELECT * FROM tienda";
			$result =mysqli_query($this->link,$query);
			$data   =array();
			while ($data[]=mysqli_fetch_assoc($result));
			array_pop($data);
			return $data;
		}

		//Crear una nueva tienda
		public function newTienda($data){
			$query  ="INSERT INTO tienda ( localizacion, fecha_apertura, nombre) VALUES ('".$data['localizacion']."','".$data['fecha_apertura']."','".$data['nombre']."')";
			$result =mysqli_query($this->link,$query);
			if(mysqli_affected_rows($this->link)>0){
				return true;
			}else{
				return false;
			}
		}

		//Obtener tienda por id
		public function getTiendaById($id_tienda=NULL){
			if(!empty($id_tienda)){
				$query  ="SELECT * FROM tienda WHERE id_tienda=".$id_tienda;
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
			}else{
				return false;
			}
		}

		//Actulizar tienda por id
		public function setEditTienda($data){
			if(!empty($data['id_tienda'])){
				$query  ="UPDATE tienda SET localizacion='".$data['localizacion']."',fecha_apertura='".$data['fecha_apertura']."', nombre='".$data['nombre']."' WHERE id_tienda=".$data['id_tienda'];
				$result =mysqli_query($this->link,$query);
				if($result){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Borra la tienda por id
		public function deleteTienda($id_tienda=NULL){
			if(!empty($id_tienda)){
				$query  ="DELETE FROM tienda WHERE id_tienda=".$id_tienda;
				$result =mysqli_query($this->link,$query);
				if(mysqli_affected_rows($this->link)>0){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

	}
