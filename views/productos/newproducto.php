<?php

include_once './models/tienda.php';

	$tienda  = new Tienda();

	
		//Trae todos las tiendas
		$tiendas = $tienda->getTienda();
	

	$title="Listado de productos";
	
?>
<form id="newproducto"action="./controllers/producto_controller.php" method="POST">
  <div class="form-group">
  	 <label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Nombre del producto" required>
  </div>
  <div class="form-group">
  	 <label for="sku">SKU</label>
    <input type="int" class="form-control" id="sku" name="sku" placeholder="ingrese el codigo para el producto" required>
  </div>
  <div class="form-group">
  	 <label for="descripcion">Descripcion</label>
    <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripcion del producto" required>
  </div>
  <div class="form-group">
      <label for="valor">Valor:</label>
      <input type="double" class="form-control" id="valor" name="valor" placeholder="valor del producto" required>
  </div>
  <div>
      <label for="form-group">Tienda destino</label>
      <select name="tienda_fk" id="tienda_fk"class="form-control"  placeholder="selecciona la tienda" required>
      <option value="0">Selecciona una tienda de destino</option>
      <?php foreach ($tiendas as $tienda) { ?>
        <option value="<?php echo $tienda['id_tienda']; ?>"><?php echo $tienda['nombre']?></option> 
      <?php } ?>
      </select>
  </div>
  <br>
  <div class="form-group text-center">
  	<input type="submit" name="create" value="Crear" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				El producto ha sido registrada.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al crear el producto, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
</form>


