<?php

  include './models/producto.php';
  $title="Listado de productos";

  $producto     = new Producto();
  $sku       = isset($_GET['sku'])?$_GET['sku']:null;
  $productos    = $producto->getProductoById($sku);
  $descripcion     = '';
  $valor = '';
  $nombre    = '';

  if($productos){
    $nombre    = $productos[0]['nombre'];
    $descripcion   = $productos[0]['descripcion'];
    $valor = $productos[0]['valor'];
    
  }

?>
<form action="./controllers/producto_controller.php" method="POST">
  <div class="form-group">
  	 <label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre del producto" autofocus required value="<?php echo $nombre; ?>">
  </div>
  <div class="form-group">
  	 <label for="descripcion">descripcion</label>
    <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripcion" required value="<?php echo $descripcion; ?>">
  </div>
  <div class="form-group">
  	 <label for="valor">valor</label>
    <input type="double" class="form-control" id="valor" name="valor" placeholder="Valor de producto" required value="<?php echo $valor; ?>">
  </div>
  <div class="form-group text-center">
  	<input type="submit" name="editproducto" value="Editar" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				La informacion ha sido actualizada.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al editar la información, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
  <input type="hidden" name="sku" value="<?php echo $sku ?>">
</form>