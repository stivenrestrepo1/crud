<?php

	include './models/producto.php';

	$producto  = new Producto();

	
		//Trae todos los productos
		$dataSearch=NULL;
		$producto =$producto->getProducto();
	

	$title="Listado de productos";
?>
<div class="row">
	<div class="col text-center">
	<h2>Listado de productos</h2>
		<i class="material-icons" style="font-size: 80px;">list_alt</i>
	</div>
</div>
<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead class="thead-dark">
				<th class="text-center">SKU</th>
				<th class="text-center">Nombre</th>
				<th class="text-center">Descripcion</th>
				<th class="text-center">valor</th> 
				<th class="text-center">Tienda destino</th> 
				<th class="text-center">Acciones</th>
			</thead>
			<tbody>
				<?php

					if(count($producto)>0){

						foreach ($producto as $column =>$value) {
				?>

							<tr class="text-center" id="row<?php echo $value['sku']; ?>">
								<td ><?php echo $value['sku']; ?></td>
								<td><?php echo $value['nombre']; ?></td>
								<td><?php echo $value['descripcion']; ?></td>
                                <td><?php echo $value['valor']; ?></td>
                                <td><?php echo $value['tienda_fk']; ?></td>
								<td class="text-center">
									<a href="./index.php?page1=editproducto&sku=<?php echo $value['sku'] ?>" title="Editar producto: <?php echo $value['nombre'].' '.$value['descripcion'].' '.$value['valor'] ?>">
										<i class="material-icons btn_edit">edit</i>
									</a>
									<a href="#" onclick="btnDelete(<?php echo $value['sku'] ?>)" id="btnDelete" title="Borrar producto: <?php echo $value['nombre'].' '.$value['descripcion'].' '.$value['valor'].' '.$value['tienda_fk'] ?>">
										<i class="material-icons btn_delete">delete_forever</i>
									</a>
								</td>
							</tr>
				<?php
						}
					}else{
				?>
					<tr>
						<td colspan="5">
							<div class="alert alert-info">
								No se encontraron productos.
							</div>
						</td>
					</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col">
			<div class="alert alert-success" id="msgSuccess" style="display: none;"></div>
			<div class="alert alert-danger" id="msgDanger" style="display: none;"></div>
		</div>
	</div>
<script type="text/javascript">

	function btnDelete(sku){
		if(confirm("Esta seguro de eliminar el producto?")){
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function(){
			if (this.readyState == 4 && this.status == 200) {
				var response   = JSON.parse(this.responseText);
				var msgSuccess = document.getElementById('msgSuccess');
				var msgDanger   = document.getElementById('msgDanger');
				if(response.success){
					msgSuccess.style.display = 'inherit';
					msgSuccess.innerHTML     = 'El producto ha sido borrado de la base de datos.';
					msgDanger.style.display  = 'none';

					//Elimina el registro de la tabla
					var row    = document.getElementById('row'+sku);
					var parent = row.parentElement;
        			parent.removeChild(row);

				}else if(response.error){
					msgDanger.style.display  = 'inherit';
					msgDanger.innerHTML      = 'No se ha podido eliminar el producto';
					msgSuccess.style.display = 'none';
				}
			}
			};
			xhttp.open("GET", "./controllers/producto_controller.php?delete=true&sku="+sku, true);
			xhttp.send();
		}
	}


</script>