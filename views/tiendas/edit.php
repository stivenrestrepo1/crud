<?php

  include './models/tienda.php';
  $title="Listado de Tiendas";

  $tienda     = new Tienda();
  $id_tienda       = isset($_GET['id_tienda'])?$_GET['id_tienda']:null;
  $tiendas    = $tienda->getTiendaById($id_tienda);
  $localizacion     = '';
  $fecha_apertura = '';
  $nombre    = '';
  if($tiendas){
    $nombre    = $tiendas[0]['nombre'];
    $localizacion   = $tiendas[0]['localizacion'];
    $fecha_apertura = $tiendas[0]['fecha_apertura'];
    
  }

	include 'toolbar.php';
?>
<form action="./controllers/tienda_controller.php" method="POST">
  <div class="form-group">
  	 <label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre de la tienda" autofocus required value="<?php echo $nombre; ?>">
  </div>
  <div class="form-group">
  	 <label for="localizacion">localizacion</label>
    <input type="text" class="form-control" id="localizacion" name="localizacion" placeholder="Geolocalizacion" required value="<?php echo $localizacion; ?>">
  </div>
  <div class="form-group">
  	 <label for="date">fecha apertura</label>
    <input type="date" class="form-control" id="fecha_apertura" name="fecha_apertura" placeholder="fecha de la apertura" required value="<?php echo $fecha_apertura; ?>">
  </div>
  <div class="form-group text-center">
  	<input type="submit" name="edit" value="Editar" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				La informacion ha sido actualizada.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al editar la información, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
  <input type="hidden" name="id_tienda" value="<?php echo $id_tienda ?>">
</form>