<?php

	include './models/tienda.php';

	$tienda  = new Tienda();

	
		//Trae todos las tiendas
		$dataSearch=NULL;
		$tienda =$tienda->getTienda();
	

	$title="Listado de tiendas";
	include 'toolbar.php';
?>
<div class="row">
	<div class="col text-center">
	<h2>Listado de tiendas</h2>
		<i class="material-icons" style="font-size: 80px;">store_mall_directory</i>
	</div>
</div>
<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead class="thead-dark">
				<th class="text-center">Id</th>
				<th class="text-center">Nombre</th>
				<th class="text-center">Geolocalizacion</th>
				<th class="text-center">Fecha de apertura</th> 
				
				<th class="text-center">Acciones</th>
			</thead>
			<tbody>
				<?php

					if(count($tienda)>0){

						foreach ($tienda as $column =>$value) {
				?>

							<tr class="text-center" id="row<?php echo $value['id_tienda']; ?>">
								<td ><?php echo $value['id_tienda']; ?></td>
								<td><?php echo $value['nombre']; ?></td>
								<td><?php echo $value['localizacion']; ?></td>
								<td><?php echo $value['fecha_apertura']; ?></td>
								<td class="text-center">
									<a href="./index.php?page=edit&id_tienda=<?php echo $value['id_tienda'] ?>" title="Editar tienda: <?php echo $value['nombre'].' '.$value['localizacion'].' '.$value['fecha_apertura'] ?>">
										<i class="material-icons btn_edit">edit</i>
									</a>
									<a href="#" onclick="btnDelete(<?php echo $value['id_tienda'] ?>)" id="btnDelete" title="Borrar tienda: <?php echo $value['nombre'].' '.$value['localizacion'].' '.$value['fecha_apertura'] ?>">
										<i class="material-icons btn_delete">delete_forever</i>
									</a>
								</td>
							</tr>
				<?php
						}
					}else{
				?>
					<tr>
						<td colspan="5">
							<div class="alert alert-info">
								No se encontraron tiendas.
							</div>
						</td>
					</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col">
			<div class="alert alert-success" id="msgSuccess" style="display: none;"></div>
			<div class="alert alert-danger" id="msgDanger" style="display: none;"></div>
		</div>
	</div>
<script type="text/javascript">

	function btnDelete(id_tienda){
		if(confirm("Esta seguro de eliminar el registro?")){
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function(){
			if (this.readyState == 4 && this.status == 200) {
				var response   = JSON.parse(this.responseText);
				var msgSuccess = document.getElementById('msgSuccess');
				var msgDanger   = document.getElementById('msgDanger');
				if(response.success){
					msgSuccess.style.display = 'inherit';
					msgSuccess.innerHTML     = 'la tienda ha sido borrado de la base de datos.';
					msgDanger.style.display  = 'none';

					//Elimina el registro de la tabla
					var row    = document.getElementById('row'+id_tienda);
					var parent = row.parentElement;
        			parent.removeChild(row);

				}else if(response.error){
					msgDanger.style.display  = 'inherit';
					msgDanger.innerHTML      = 'No se ha podido eliminar el registro';
					msgSuccess.style.display = 'none';
				}
			}
			};
			xhttp.open("GET", "./controllers/tienda_controller.php?delete=true&id_tienda="+id_tienda, true);
			xhttp.send();
		}
	}


</script>