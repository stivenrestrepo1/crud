<?php
	include 'toolbar.php';
?>
<form action="./controllers/tienda_controller.php" method="POST">
  <div class="form-group">
  	 <label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Nombre del establecimiento" required>
  </div>
  <div class="form-group">
  	 <label for="localizacion">Geolocalizacion</label>
    <input type="text" class="form-control" id="localizacion" name="localizacion" placeholder="Geolocalizacion" required>
  </div>
  <div class="form-group">
  	 <label for="fecha_apertura">Fecha de apertura</label>
    <input type="date" class="form-control" id="fecha_apertura" name="fecha_apertura" placeholder="Fecha de apertura" required>
  </div>
  <div class="form-group text-center">
  	<input type="submit" name="create" value="Crear" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				La tienda ha sido registrada.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al crear la tienda, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
</form>